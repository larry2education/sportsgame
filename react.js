//Creating a Stateful Compontent with Info of the team with Props
class Team extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
        
        reset: 0,      
        count: 0,
        score: 0
        
        };
    }
    

shotsTaken = () => {//FG Attempt Counter.. Also houses methods to help build the Team Component
    this.buckets()
    setTimeout(this.shotPercentage, 0)
    //console.log("shooting")
    this.setState((state) => 
    {
        return{...state, count: state.count + 1}  
    });
    
}

onReset = () => { //creating reset method for team to restart game
    //console.log("reset")
    this.setState((state) =>
 {
     return{...state, count: 0, score: 0, percentage: 0}
 });
    this.resetPercentage()
}

buckets = () => { //generating a random number to simalate shots made
    
    let chance = (Math.ceil(Math.random() * 3));
    //console.log(chance)
    if (chance === 3) {
        this.setState ((state) => {
            //console.log(this.state.score)
            return {...state, score: state.score + 1 }
        
        })
    };
    let swish = new Audio ('./fx/swish.wav');//created if-else statements to be able to generate sounds
    let point = new Audio ('./fx/steelsword.wav');//playing the swish sound for every shot taken and point sound for each score
    
    if (chance === 3){ 
        swish.play(); //https://medium.com/@jerrysbusiness/playing-a-sound-file-in-a-react-project-bd0ad079ad93
    }
    else {
        point.play();
    }

}

shotPercentage = () => { //creating shot percentage that divides score by count
this.setState((state) => {
    return {...state, percentage: Math.round((state.score / state.count) * 100) + "%"}
});
document.querySelector(".Percentage").textContent = this.state.percentage
}

resetPercentage = () => { // hiding display of shot percentage
    document.querySelector(".Percentage").textContent = ""
}
theSounds = () => { 
    
     
    //console.log("swish")
}


render(){ //displaying all information in team Component
    const hometeam = "Memphis Grizzlies"
    const hometeamLogo = "./img/grizzlieslogo.png"
    
  
    return (
        <React.Fragment>
            <h3>{hometeam}</h3>
            <img src = {hometeamLogo}></img>
            <div>
            <p>FG Attempts - {this.state.count}</p> 
            <p>Score - {this.state.score}</p>
            <p className = "Percentage"></p> 
            </div>          
            <button onClick={this.shotsTaken}>Shoot</button>
            <button onClick={this.onReset}>RESET</button> 
        </React.Fragment>
        );
    }
     
}

//Basically a Rinse and Repeat of the previous Team Class
class OtherTeam extends React.Component {
    constructor(props){
        super(props);
        this.state = {
        count: 0,
        score: 0
        };
        
    }

shotTaken = () => {//FG Attempt Counter.. Also houses methods to help build the Team Component
    this.buckets()
    setTimeout(this.shotPercentage, 0)
    //console.log("shooting")
    this.setState((state) => 
    {
        return{...state, count: state.count + 1}  
    });
    
}

onReset = () => { //creating reset method for team to restart game
    //console.log("reset")
    this.setState((state) =>
 {
     return{...state, count: 0, score: 0, percentage: 0}
 });
    this.resetPercentage()
}

buckets = () => { //generating a random number to simalate shots made
    
    let chance = (Math.ceil(Math.random() * 3));
    //console.log(chance)
    if (chance === 3) {
        this.setState ((state) => {
            //console.log(this.state.score)
            return {...state, score: state.score + 1 }
        
        })
    };
    let swish = new Audio ('./fx/swish.wav');//created if-else statements to be able to generate sounds
    let point = new Audio ('./fx/steelsword.wav');//playing the swish sound for every shot taken and point sound for each score
    
    if (chance === 3){ 
        swish.play(); //https://medium.com/@jerrysbusiness/playing-a-sound-file-in-a-react-project-bd0ad079ad93
    }
    else {
        point.play();
    }

}

shotPercentage = () => { //creating shot percentage that divides score by count
this.setState((state) => {
    return {...state, percentage: Math.round((state.score / state.count) * 100) + "%"}
});
document.querySelector(".Percent").textContent = this.state.percentage
}

resetPercentage = () => { // hiding display of shot percentage
    document.querySelector(".Percent").textContent = ""
}
theSounds = () => { 
    
     
    //console.log("swish")
}
 

render(){

    const visitorTeam = "San Antonio Spurs"
    const visitorLogo = "./img/spurslogo.png"
    return (
        <React.Fragment>
            <h3>{visitorTeam}</h3>
            <img src={visitorLogo}></img>
            <div> 
            <p>FG Attempts - {this.state.count}</p> 
            <p>Score - {this.state.score}</p>
            <p className = "Percent"></p> 
            </div>
            <button onClick={this.shotTaken}>Shoot</button>
            <button onClick={this.onReset}>RESET</button>
        </React.Fragment>
        );
    }    
}

class Game extends React.Component {
        
    
    render(){
        const venue = "Grind House at FedEx Forum"
        return (
        <React.Fragment>
            <h1>Welcome to the {venue}!!!</h1>
            <Team />
            <OtherTeam />
        </React.Fragment>
        ); 
    }
}
class App extends React.Component {
    render(){
        return (
            <React.Fragment>
                <Game />
            </React.Fragment>
        );
    }
}


//Render the Root!!
ReactDOM.render(<App />, document.getElementById('root'));
  